%\documentclass[handout]{beamer}
\documentclass{beamer}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{proof}
\usepackage{hyperref}
\usepackage{stmaryrd} % for \llbracket, \llbracket

\input{abbrevs.tex}

\begin{document}

\begin{frame}[plain]
  \begin{center}
    \vspace{2cm}
    \textcolor{blue}{
      {\Large Gluing for type theory}
    }
    
    \vspace{0.5cm}

    Ambrus Kaposi, Simon Huber, Christian Sattler
        
    \vspace{1cm}

    FSCD \\

    \vspace{1cm}

    26 June 2019
    
  \end{center}
\end{frame}

\begin{frame}{Overview}
\tableofcontents
\end{frame}

\AtBeginSection[]
  {
     \begin{frame}<beamer>
     \frametitle{Plan}
     \tableofcontents[currentsection]
     \end{frame}
  }

\section{Background}

\begin{frame}{Logical relation arguments}
  are used to prove properties of type theories such as

  \bigskip

  \begin{itemize}
  \item injectivity of type formers \cite{Abel2017DecidabilityOC}
  \item canonicity \cite{DBLP:journals/corr/abs-1810-09367}
  \item normalisation \cite{nbe}
  \item definability \cite{plotkin73}
  \item parametricity \cite{reynolds83abstraction}
  \end{itemize}

  \bigskip \pause

  Our goal: generalise these results.
  
\end{frame}

\begin{frame}{Gluing}
  originates from Artin gluing of Grothendieck toposes \cite{sga4-i}.

  \bigskip
  
  Given a functor $F : \mathcal{C} \ra \mathcal{D}$, gluing over $F$
  is a new category in which objects are triples:
  \[
    \Gamma :|\mathcal{C}|\text{, }\Delta:|\mathcal{D}|\text{ and a morphism } \mathcal{D}(\Delta, F\,\Gamma)
  \]

  \pause
  
  Models of type theories can be seen as a categories with extra
  structure and gluing can be extended to these to prove e.g.
  \begin{itemize}
  \item normalisation for simple type theory \cite{DBLP:conf/ppdp/Fiore02,DBLP:journals/corr/abs-1809-08646}
  \item normalisation for System F \cite{alti:f97}
  \item homotopy canonicity for 1-truncated HoTT \cite{shulman:invdia}
  \end{itemize}

  \pause

  In this talk we do the same for dependent type theory.
\end{frame}

\section{Algebraic type theory}

\begin{frame}{Warmup: ``algebraic'' natural numbers}
  Natural numbers are specified by the algebraic theory with one sort
  $\mathsf{N}:\Set$ and two operators $\mathsf{z}:\mathsf{N}$,
  $\mathsf{s}:\mathsf{N}\ra\mathsf{N}$.
  \begin{itemize}
  \item A \emph{model} of natural numbers is an algebra: a pointed set
    with an endofunction.
    \pause
  \item A \emph{morphism} from $(A,u,f)$ to $(B,v,g)$ is a
    function $\alpha:A\ra B$
    s.t.\ $\alpha\,u = v$ and
    $\forall x . \alpha\,(f\,x) = g\,(\alpha\,x)$.
    \pause
  \item A \emph{displayed model} over $(A,u,f)$ is a family
    $P:A\ra\Set$ with an element of $P\,u$ and a function
    $\forall x . P\,x\ra P\,(f\,x)$ (sometimes $P$ is called motive
    and the other two methods).
    \pause
  \item A \emph{section} of a displayed model $(P,u,g)$ over
    $(A,u,f)$ is a dependent function $\alpha:(x:A)\ra P\,x$ s.t.\
    $\alpha\,u = v$ and $\forall x . \alpha\,(f\,x) = g\,(\alpha\,x)$.
    \pause
  \item The \emph{syntax} is a model s.t.
   \begin{itemize}
   \item there is a unique morphism from it to any model
     (\emph{recursor}),
   \item there is a section into any displayed model over the syntax
     (\emph{eliminator}).
   \end{itemize}
%   \item A \emph{morphism} from $\mathcal{A}$ to $\mathcal{B}$ is a
%     function $f:\mathsf{N}_{\mathcal{A}}\ra\mathsf{N}_{\mathcal{B}}$
%     s.t.\ $f\,\mathsf{z}_{\mathcal{A}} = \mathsf{z}_{\mathcal{B}}$ and
%     $\forall n . f\,(\mathsf{s}_{\mathcal{A}}\,n) =
%     \mathsf{s}_{\mathcal{B}}\,(f\,n)$.
%   \item A \emph{displayed model} $\mathcal{P}$ over $\mathcal{A}$ is a
%     family
%     $\mathsf{N}_{\mathcal{P}} : \mathsf{N}_{\mathcal{A}}\ra\Set$
%     together with an element of
%     $\mathsf{N}_{\mathcal{P}}\,\mathsf{z}_{\mathcal{A}}$ and a
%     function
%     $(n:\mathsf{N}_{\mathcal{A}})\ra\mathsf{N}_{\mathcal{P}}\,n\ra\mathsf{N}_{\mathcal{P}}\,(\mathsf{s}_{\mathcal{A}}\,n)$
%     (motive and methods of the eliminator).
  \end{itemize}
\end{frame}

\begin{frame}{Algebraic type theory}
  Type theory (with $\Pi$, $\Sigma$, $\U$, $\Bool$) is a generalised
  algebraic theory with 4 sorts\footnote{In the paper we index $\Con$
    and $\Ty$ by universe levels.}
  \begin{alignat*}{10}
    & \Con && :\,\, && \Set                  && \Sub && :\,\, && \Con\ra\Con\ra\Set \\
    & \Ty  && : && \Con\ra\Set \hspace{4em}  && \Tm  && : && (\Gamma:\Con)\ra\Ty\,\Gamma\ra\Set,
  \end{alignat*}
  
  \pause

  26 operators e.g.
  \begin{alignat*}{5}
    & \blank\ext\blank && :\,\, && (\Gamma:\Con)\ra\Ty\,\Gamma\ra\Con \\
    & \lam  && : && \Tm\,(\Gamma\ext A)\,B\ra\Tm\,\Gamma\,(\Pi\,A\,B),
  \end{alignat*}

  \pause
  
  and 34 equations e.g.
  \[
   {\Pi\beta} : (\lam\,t)\oldapp u = t[\id,u].\hspace{5em}
  \]

  \pause

  This is the same as a CwF with extra structure.
\end{frame}

\begin{frame}{Algebraic type theory}
  \begin{itemize}
  \item A \emph{model} of type theory is an algebra (4 families of
    sets, 26 functions and 34 equalities).
  \item A \emph{morphism} of models is given by 4 functions between
    the corresponding sorts and 26 equalities.
  \item A \emph{displayed model} indexed over a model $\mathcal{M}$
    encodes a model together with a morphism into $\mathcal{M}$ (4
    families, 26 functions, 34 equalities).
  \item A \emph{section} of a displayed model is like a dependent
    morphism (4 dependent functions and 26 equalities).
  \item The \emph{syntax} $\mathsf{S}$ is a model s.t.
    \begin{itemize}
    \item there is a unique morphism from it to any other model called
      the \emph{recursor},
    \item there is a section into any displayed model over
      $\mathsf{S}$ called the \emph{eliminator}.
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{An example: the $\mathsf{Set}$ model}
  Some components of the $\Set$ model:
  \begin{alignat*}{5}
    & \Con_\Set && := \Set \\
    & \Ty_\Set\,\Gamma && := \Gamma\ra\Set \\
    & \Tm_\Set\,\Gamma\,A && := (\gamma:\Gamma)\ra A\,\gamma \\
    & \Gamma\ext_\Set A && := (\gamma:\Gamma)\times A\,\gamma \\
    & \Pi_\Set\,A\,B && := \lambda\gamma.(\alpha:A\,\gamma)\ra B\,\gamma \\
    & \lam_\Set\,t && := \lambda\gamma.\lambda\alpha.t\,(\gamma,\alpha)
  \end{alignat*}
  The recursor gives us a strict morphism from the syntax $\S$ to the
  above model $\Set$. We can use this to define an interpreter:
  \begin{alignat*}{5}
    & \llbracket\blank\rrbracket && : \Con_\S\ra\Set && := \mathsf{rec}^\Set_\Con \\
    & \llbracket\blank\rrbracket && : \Ty_\S\,\Gamma\ra\llbracket\Gamma\rrbracket\ra\Set && := \mathsf{rec}^\Set_\Ty \\
    & \llbracket\blank\rrbracket && : \Tm_\S\,\Gamma\,A\ra(\gamma:\llbracket\Gamma\rrbracket)\ra\llbracket A\rrbracket\,\gamma && := \mathsf{rec}^\Set_\Tm
  \end{alignat*}
\end{frame}

\section{Pseudomorphisms}

\begin{frame}{Pseudomorphism}
  A \emph{pseudomorphism} $F$ from $\mathcal{C}$ to $\mathcal{D}$
  consists of:
  \begin{itemize}
  \item 4 maps $F:\Con_{\mathcal{C}}\ra\Con_{\mathcal{D}}$, $F :\Ty_{\mathcal{C}}\,\Gamma\ra\Ty_{\mathcal{D}}\,(F\,\Gamma)$, $\dots$
  \item which respect $\id$, $\circ$ and substitutions strictly, e.g.\
    $F\,\id=\id$, $F\,(\sigma\circ\delta) = F\,\sigma\circ_ F\,\delta$,
    $F\,(A[\sigma])=F\,A[F\,\sigma]$,
  \item and respect the empty context and context extension up to
    isomorphism in $\mathcal{D}$, e.g.\
    $F\,(\Gamma\ext A) \cong F\,\Gamma\ext F\,A$,
  \item no conditions on other type/term formers.
  \end{itemize}

  \pause

  Surprise: $F$ automatically preserves type formers as follows:
  \begin{itemize}
  \item $\Sigma$ is preserved weakly: $F\,(A\times B) \cong F\,A\times F\,B$
  \item $\Pi$ and $\U$ lax: $F_{\Pi.1}:\Tm_{\mathcal{D}}\,\big(F\,\Gamma\ext F\,(A\Ra B)\big)\,(F\,A\Ra F\,B)$
  \item $\Bool$ op-lax: $F_{\Bool.2}:\Tm_{\mathcal{D}}\,(F\,\Gamma\ext\Bool)\,(F\,\Bool)$
  \end{itemize}
\end{frame}

\begin{frame}{Examples of morphisms}
  Any strict morphism is a pseudomorphism e.g.\ identities, recursor.

  \bigskip \pause
  
  The global section functor $\G : \S \ra \Set$
  \begin{alignat*}{5}
    & \G\,\Gamma && := \Sub_\S\,\cdot\,\Gamma \\
    & \G\,A && := \lambda\rho.\Tm_\S\,\cdot\,(A[\rho])
  \end{alignat*}
  is not strict as
  \[
    \G\,(\Gamma\ext A) = \Sub_\S\,\cdot\,(\Gamma\ext A) \neq (\rho:\Sub_\S\,\cdot\,\Gamma)\times \Tm_\S\,\cdot\,(A[\rho]) = \G\,\Gamma \ext_\Set\G\,A
  \]
  but is pseudo.
  
  \bigskip \pause
  
  The Yoneda embedding from $\S$ to the presheaf model over a wide
  subcategory of $\S$ is not strict but is pseudo.
\end{frame}

\begin{frame}{$\Pi$ is not preserved weakly by $\G$}
  \[
    \Tm_{\mathcal{D}}\,\big(F\,\Gamma\ext F\,(A\Ra B)\big)\,(F\,A\Ra F\,B)
  \]
  can be defined, but the other direction not, because it would imply
  \[
    \Tm_{\Set}\,(\G\,\cdot\ext\, {\G\,\Nat\Ra\G\,\Bool})\,\big(\G\,(\Nat\Ra \Bool)\big),
  \]
  which translates to
  \[
    (\Tm\,\cdot\,\Nat\ra\Tm\,\cdot\,\Bool)\ra\Tm\,\cdot\,(\Nat\Ra\Bool)
  \]
  but there are more metatheoretic $\Nat\ra\Bool$ functions than terms
  of this type.
  
\end{frame}

\section{Gluing}

\begin{frame}{Gluing in an indexed way}
  Given a pseudomorphism $F :\mathcal{C}\ra\mathcal{D}$ we define the
  glued model $\P$.

  \pause
  
  Artin gluing would be:
  \begin{alignat*}{5}
    & \Con_{\P} := (\Gamma :\Con_{\mathcal{C}})\times(\Delta:\Con_{\mathcal{D}})\times\Sub_{\mathcal{D}}\,\Delta\,(F\,\Gamma) \\
    & \Sub_\P\,(\Gamma,\Delta,\sigma)\,(\Gamma',\Delta',\sigma') := \\
    & \hspace{2em} (\gamma:\Sub_{\mathcal{C}}\,\Gamma\,\Gamma')\times(\delta:\Sub_{\mathcal{D}}\,\Delta\,\Delta')\times(\sigma'\circ\delta = F\,\gamma\circ\sigma)
  \end{alignat*}
%  Then we have a strict morphism from $\P$ to $\mathcal{C}$ which
%  projects out the first component.
%  \bigskip
  
  \pause

  An alternative is to define a displayed model over $\Gamma$:
  \[
    \Con_{\P}\,\Gamma := (\Delta:\Con_{\mathcal{D}})\times\Sub_{\mathcal{D}}\,\Delta\,(F\,\Gamma)
  \]

  \pause

  In fact we use an even more indexed version:
  \[
    \Con_{\P}\,\Gamma := \Ty_{\mathcal{D}}\,(F\,\Gamma)
  \]
  This way there are no equalities in the model, everything is
  indexed, substitutions are just terms:
  \[
    \Sub_\P\,\Gamma_\P\,\Delta_\P\,\sigma := \Tm\,(F\,\Gamma\ext\Gamma_\P)\,(\Delta_\P[F\,\sigma])
  \]
\end{frame}

\begin{frame}{Some parts of the gluing model}
  The disadvantage of indexing: $\mathcal{D}$ needs $\top$, $\Sigma$
  types in addition to what type formers $\mathcal{C}$ has. We use
  logical predicate intutition.

  \pause
  
  The predicate holds at an extended context if it both holds at the smaller context and the type. We express this using a $\Sigma$ type:
  \[
    \Gamma_\P\ext_\P A_\P := \Sigma\,\big(\Gamma_\P[\p\circ F_{\ext.1}]\big)\,\big(A_\P[\p\circ F_{\ext.1}\circ\p,0,\q[F_{\ext.1}\circ\p]]\big)
  \]

  \pause
  
  The predicate holds at a function type if the predicates are preserved:
  \begin{alignat*}{5}
    & \Pi_\P\,A_\P\,B_\P && := \Pi\,\big(F\,A[\p^2]\big)\,\\
    & && \hphantom{:= \Pi\,\,} \Big(\Pi\,\big(A_\P[\p^2,\q]\big)\,\big(B_\P\big[F_{\ext.2}\circ(\p^4,1), (3,0), F_{\Pi.1}[\p^4,2]\oldapp 1\big]\big)\Big)
  \end{alignat*}
  
  \pause

  The predicate holds for an $F\,\Bool$ if there is a boolean the
  $F$-image of which is equal to it ($\mathcal{D}$ also needs $\Id$
  types):
  \[
    \Bool_\P := \Sigma\,\Bool\,\big(\Id\,(F_{\Bool.2}[\p^3,\q])\,1\big)
  \]
  
  \pause
  
  Terms preserve the predicate:
  \[
    \Tm_\P\,\Gamma_\P\,A_\P\,t := \Tm\,(F\,\Gamma\ext\Gamma_\P)\,(A_\P[\id,F\,t])
  \]
\end{frame}

\begin{frame}{Applications}
  Identity on $\S$: Bernardy style \cite{bernardy12parametricity}
  parametricity.

  \bigskip \pause
  
  Set interpretation: Reynolds style \cite{reynolds83abstraction}
  parametricity.
  
  \bigskip \pause
  
  Global section: canonicity: a term $t$ of type bool in the empty
  context preserves the predicate, by gluing we obtain
  \[
    (b:\Bool_\Set)\times (\IF_\Set\,b\,\THEN\,\true_\S\,\ELSE\,\false_\S = t).
  \]

  \pause

  We formalised the glued model in Agda using shallow embedding
  \cite{shallow}. All equalities in the model and the pseudomorphism
  are definitional. All equalities in the glued model are
  definitional.
  
  \bigskip \pause

  The identity type can also be glued, even extensional identity works
  (ETT has canonicity).

  \bigskip \pause
  
  Further work: gluing over Yoneda gives normalisation but we need to
  extend gluing with a quote function.
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{References}
  \bibliographystyle{amsalpha}
  \bibliography{b}
\end{frame}

\end{document}
